import { StyleSheet, Text, View, StatusBar } from 'react-native';
import Screens from './src/screens'
import { getApps, initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAdOverDjmaAqcsQhPOoJX42fj_woqclL4",
  authDomain: "kamilapp-f3306.firebaseapp.com",
  databaseURL: "https://kamilapp-f3306-default-rtdb.firebaseio.com",
  projectId: "kamilapp-f3306",
  storageBucket: "kamilapp-f3306.appspot.com",
  messagingSenderId: "57249685430",
  appId: "1:57249685430:web:49d0816323a377255aab17",
  measurementId: "G-9FCYM10X49"
};

// Initialize Firebase
if(!getApps.length){
  initializeApp(firebaseConfig);
}

export default function App() {
  return (
    <>
    <StatusBar/>
    <>
      <Screens/>
    </>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

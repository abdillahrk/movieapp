import axios from 'axios'
import * as SecureStore from "expo-secure-store";

const ROOT_URL = "https://api.themoviedb.org/3";
const IMAGE_URL = "https://image.tmdb.org/t/p/";
export const API_KEY = "a453443dad120b1433744ea45b95a0a9";


export const getImageUrl = (path, key = "uri", width = "w500") => {
    return { [key]: `${IMAGE_URL}${width}${path}` };
};

export const client = axios.create({
	baseURL: ROOT_URL,
});

client.interceptors.request.use(async (config) => {
	const token = await SecureStore.getItemAsync("token");

	if (token) {
		// eslint-disable-next-line no-param-reassign
		config.headers.Authorization = `Bearer ${token}`;
	}

	return config;
});

export const fetchMovies = async (movies) =>{
    const response = await client.get(`/movie/550?api_key=${API_KEY}`);
    return [...movies, ...response.data.results]
}


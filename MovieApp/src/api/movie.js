import { client } from "./client";

const API_KEY = "a453443dad120b1433744ea45b95a0a9";

export const getPopularMovie = async () => client.get(`/movie/550?api_key=${API_KEY}`)

export const getTopRatedMovie = async () => client.get(`/movie/top_rated?api_key=${API_KEY}&language=en-US&page=1`)

export const getUpComingMovie = async () => client.get(`/movie/upcoming?api_key=${API_KEY}&language=en-US&page=1`)
import { View, Text, TextInput, StyleSheet } from "react-native";
import React from "react";
import Scale from "../transforms/Scale";

const Form = ({label, placeholder, value, onChangeText}) => {
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    color: "#ABABAB",
    fontSize: Scale(18),
    fontWeight: "bold",
    marginHorizontal: Scale(10),
  },
  input: {
    backgroundColor: "#DEDEDE",
    borderWidth: Scale(1),
    marginHorizontal: Scale(10),
    height: Scale(50),
    borderRadius: Scale(10),
    color: "black",
    padding: Scale(10),
    marginBottom: Scale(15),
  },
});

export default Form;

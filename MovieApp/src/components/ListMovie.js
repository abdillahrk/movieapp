import { StyleSheet, Text, View, ScrollView, FlatList, TouchableOpacity, Image, TouchableWithoutFeedback, TouchableNativeFeedback } from 'react-native'
import React from 'react'
import { interpolateNode } from 'react-native-reanimated'
import { getImageUrl } from '../api/client'
import FastImage from "react-native-fast-image";
import Scale from '../transforms/Scale'
import { useNavigation } from '@react-navigation/native';

const ListMovie = ({title,data}) => {
    const navigation = useNavigation();
  return (
    <View>
      <View style={{margin: 8}}>
          <Text style={styles.title}>{title}</Text>
      </View>
      <View>
        <FlatList
            style={styles.movielist}
            data={data}
            numColumns={3}
            keyExtractor= {(item) => item.id.toString()}
            renderItem= {({item})=>{
                return(
                    <>
                    <View  style={styles.container}>
                    <TouchableWithoutFeedback 
                        style={styles.film}
                        onPress={()=>{
                            navigation.navigate("Detail",{ id: item.id.toString() } )
                        }}
                    >
                        <View style={styles.image}>
                            <Image style={styles.image} resizeMode='cover' source={getImageUrl(item.poster_path)}/>
                        </View>
                    </TouchableWithoutFeedback>
                    </View>
                    </>
                )
            }}
            // showsHorizontalScrollIndicator={false}
        />
      </View>
    </View>
  )
}

export default ListMovie

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: Scale(18),
        fontWeight: 'bold',
        color: '#034694'
    },
    movielist: {
        
    },
    image: {
        height: Scale(180),
        width: Scale(120),
        borderRadius: Scale(15),
        margin: Scale(7),
        overflow: 'hidden'
    },
})
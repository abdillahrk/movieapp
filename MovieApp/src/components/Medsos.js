import { View, Text, StyleSheet } from "react-native";
import React from "react";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Scale from '../transforms/Scale'

const Medsos = ({icon,medsos, nama}) => {
  return (
    <View style={styles.content}>
      {icon !== null &&
        <MaterialCommunityIcons name={icon} size={24} color="black" />}
      <View style={styles.isiContent}>
        <Text style={styles.namaMedsos}>{medsos}</Text>
        <Text style={styles.namaMedsos1}>{nama}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: Scale(10),
  },
  isiContent: {
    flexDirection: "column",
    marginLeft: Scale(10),
    marginHorizontal: Scale(10),
  },
  namaMedsos: {
    fontSize: Scale(14),
    color: "#034694",
  },
  namaMedsos1: {
    fontSize: Scale(14),
  },
});
export default Medsos;

import { useEffect, useRef, useState } from 'react';

const useFetchData = () => {
  const [data, setData] = useState(null);

  const mounted = useRef(true);

  useEffect(()=>()=>{
    mounted.current = false;
  },[]);

  const fetchData = async (endPoint) => {
    try {
      const response = await fetch(endPoint);

      const { results } = await response.json();
      setData(results);
    } catch (err) {
      if(mounted.current){
        console.log(err);
      }
    }
  };

  return {
    data,
    fetchData,
  };
};

export default useFetchData;
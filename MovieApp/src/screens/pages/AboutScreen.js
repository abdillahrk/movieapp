import React, { useState } from 'react'
import { Text, View, Button, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Medsos from '../../components/Medsos';
import { useDispatch } from 'react-redux';
import { logout } from '../../redux/authSlice';
import Scale from '../../transforms/Scale'


export default function AboutScreen() {
  const dispatch = useDispatch();
  // const logout = () =>{
  //   dispatch(logout());
  // }
  return (
    <View style={styles.container}>
      <ScrollView>
        <View>
            <Image
              source={require("../../assets/image.jpg")}
              style={styles.foto}
            />
            <View style={styles.namaContainer}>
              <Text style={styles.nama}>Rendi Kamil Abdillah</Text>
            </View>
            <View style={styles.medsos}>
              <Text style={styles.title}>Media Sosial</Text>
              <Medsos
                icon="facebook"
                medsos="Facebook"
                nama="Rendi Kamil"
              />
              <Medsos 
                icon="twitter"
                medsos= "Twitter"
                nama= "rendikamil12"
              />
              <Medsos
                icon="instagram"
                medsos="Instagram"
                nama= "rendikamil12"
              />
            </View>
            <View style={styles.medsos}>
            <Text style={styles.title}>Link Portofolio</Text>
              <Medsos
                icon="gitlab"
                medsos="Gitlab"
                nama="abdillahrk"
              />
            </View>
            <View style={styles.medsos}>
              <Text style={styles.title}>Skill</Text>
              <Text style={styles.title1}>Bahasa Pemrograman</Text>
              <Medsos
                icon="language-javascript"
                medsos="Basic Javascript"
                nama="80%"
              />
              <Medsos
                icon="language-php"
                medsos="Basic PHP"
                nama="65%"
              />
              <Text style={styles.title1}>Framework/Library</Text>
              <Medsos
                icon="react"
                medsos="Basic React Native"
                nama="80%"
              />
              <Text style={styles.title1}>Database & Technology</Text>
              <Medsos
                medsos="MySQL"
                nama="75%"
              />
              <Medsos
                medsos="REST API"
                nama="75%"
              />
            </View>
            <View style={styles.logoutContainer}>
              <TouchableOpacity style={styles.logoutContainer} onPress={() => {dispatch(logout())}}>
                <Text style={styles.logout}>Logout</Text>
              </TouchableOpacity>
            </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5F5F8",
  },
  header: {
    height: Scale(57),
    justifyContent: "center",
    backgroundColor: "white",
    marginBottom: Scale(5),
  },
  daftar: {
    color: "#034694",
    marginHorizontal: Scale(20),
    fontSize: Scale(18),
    fontWeight: "bold",
  },
  foto:{
    borderRadius: Scale(90),
    height: Scale(125),
    width: Scale(125),
    alignSelf: 'center',
    marginVertical: Scale(20)
  },
  namaContainer: {
    height: Scale(56),
    backgroundColor: '#034694',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Scale(15),
  },
  nama: {
    fontSize: Scale(20),
    fontWeight: 'bold',
    color: 'white'
  },
  medsos: {
    backgroundColor: 'white',
    marginVertical: Scale(5),
    padding : Scale(10),
    borderRadius: Scale(15)
  },
  title:{
    fontSize: Scale(16),
    fontWeight: 'bold',
    color: '#034694',
    marginLeft: Scale(20)
  },
  title1:{
    fontSize: Scale(14),
    fontWeight: 'bold',
    color: '#034694',
    marginLeft: Scale(20),
    marginVertical: Scale(8)
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: Scale(10)
  },
  isiContent: {
    flexDirection: 'column',
    marginLeft: Scale(10),
    marginHorizontal: Scale(10)
  },
  namaMedsos: {
    fontSize: Scale(14),
    color: '#034694'
  },
  namaMedsos1: {
    fontSize: Scale(14),
  },
  logoutContainer: {
    height: Scale(56),
    width: Scale(300),
    backgroundColor: '#034694',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: Scale(20),
  },
  logout: {
    fontSize: Scale(20),
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center'
  },
});

import { StyleSheet, Text, View, ScrollView, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import Scale from '../../transforms/Scale'
import { client, API_KEY } from '../../api/client'
import { getImageUrl } from '../../api/client'


const DetailScreen = ({route, navigation}) => {
  const [detailMovie, setDetailMovie] = useState();



  const getDetailMovie = async () => {
    try{
      const id = route.params.id
      console.log(route.params.id);
      const res = await client.get(`/movie/${id}?api_key=${API_KEY}&language=en-US`)
      setDetailMovie(res.data)
      console.log(detailMovie);
    }catch(error){
      console.log(error);
    }
  }
  
  useEffect(()=>{
      getDetailMovie();
  },[])

  return detailMovie? (
          <ScrollView>
          <View style={styles.poster}>
              <Image style={styles.poster} resizeMode='cover' source={getImageUrl(detailMovie.backdrop_path)}/>
          </View>
          <View style={styles.content}>
              <View style={styles.image}>
              <Image style={styles.image} resizeMode='cover' source={getImageUrl(detailMovie.poster_path)}/>
              </View>
              <View style={styles.detail}>
                  <Text style={styles.title}>{detailMovie.title}</Text>
                  <Text style={styles.ratingFilm}>{detailMovie.vote_average}</Text>
                  <Text style={styles.rating}>Rating</Text>
                  <View style={{flexDirection: 'row'}}>
                      <View style={styles.genre}>
                      <Text style={styles.text}>Release Date : {detailMovie.release_date}</Text>
                      </View>
                  </View>
              </View>
          </View>
          <View style={styles.overviewContainer}>
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>Overview</Text>
              <Text>{detailMovie.overview}</Text>
          </View>       
          </ScrollView>
  ):(
      <Text>Tunggu Ya</Text>
  )
}

export default DetailScreen

const styles = StyleSheet.create({
    poster: {
        height: Scale(200),
        backgroundColor: 'powderblue'
    },
    content: {
        flexDirection: 'row'
    },
    imageContainer: {
        marginLeft: Scale(20),
        width: Scale(120),
        height: Scale(180),
        backgroundColor: 'skyblue'
    },
    image: {
        height: Scale(180),
        width: Scale(120),
        borderRadius: Scale(15),
        margin: Scale(7),
        overflow: 'hidden'
    },
    detail: {
        justifyContent: 'center',
        marginLeft: Scale(15)
    },
    title: {
        fontSize: Scale(18),
        fontWeight: 'bold',
        marginBottom: Scale(10)
    }, 
    ratingFilm: {
        fontSize: Scale(15),
        fontWeight: 'bold',
        marginBottom: Scale(5)
    },
    rating: {
        fontSize: Scale(15),
        color: '#7B7B7B',
        fontWeight: 'bold'
    },
    genre: {
        borderRadius: Scale(20),
        backgroundColor: '#C4C4C4',
        marginVertical: Scale(10),
        marginHorizontal: Scale(5)
    },
    text: {
        paddingHorizontal: Scale(10),
        paddingVertical: Scale(5),
        fontSize: Scale(15),
        fontWeight: 'bold'
    },
    overviewContainer: {
        marginHorizontal: Scale(10),
        marginVertical: Scale(20)
    }
})
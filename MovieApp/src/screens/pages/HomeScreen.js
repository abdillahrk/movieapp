import { StyleSheet, Text, View, ScrollView, Alert } from 'react-native'
import React, {useCallback, useEffect, useState} from 'react'
import ListMovie from '../../components/ListMovie'
import Carousel from '../../components/Carousel'
import { client, requestMovie, API_KEY } from '../../api/client'
import Scale from '../../transforms/Scale'


const HomeScreen = ({navigation,route}) => {

  const [popMovie, setPopMovie] = useState([]);
  const [page, setPage] = useState(1);


  const handleError = (err) => {
		console.warn("Error Status: ", err.response.data);
		Alert.alert("Gagal", err.response.data.message);
	};


  const getPopularMov = async () => {
    try{
      const res = await client.get(`/movie/popular?api_key=${API_KEY}&language=en-US&page={page}`)
      setPopMovie(res.data.results)
    }catch(error){
      handleError(error);
    }
  }
  
  // const onEndReached = async () => {
  //   try{
  //     setPage((prevPage)=>{prevPage+1})
  //     const res = await client.get(`/movie/top_rated?api_key=${API_KEY}&language=en-US&page=${page}`)
  //     if(res){
  //       setPopMovie((prevMovie)=>({}))
  //     }
  //     console.log(popMovie);
  //   }catch(error){
  //     handleError(error);
  //   }
  // }

  useEffect(()=>{
    getPopularMov()
  },[])
  

  return (
    <ScrollView>
      <View>
      <Carousel />
      <View>
        <ListMovie style={styles.list} title="Popular Movie" data={popMovie}/>
      </View>
      </View>
    </ScrollView>
  )
}

export default HomeScreen

const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
      alignItems: 'center'
    },  
    poster: {
      backgroundColor: 'black',
      height: Scale(200),
    },
    list: {
      flexDirection: 'row'
    }
})
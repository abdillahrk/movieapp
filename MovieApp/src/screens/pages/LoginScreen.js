import React, { useState } from 'react'
import { Text, View, Button, StyleSheet, TextInput, TouchableOpacity } from 'react-native'
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { useDispatch } from 'react-redux';
import { login } from '../../redux/authSlice';
import Scale from '../../transforms/Scale'

export default function LoginScreen({navigation}) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();

    const submit = () => {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        dispatch(
            login({
                token: user.stsTokenManager.accessToken
            })
        )
        navigation.navigate("Main", {
            screen: "App",
            params: {
                screen: "Home"
            }
        })
        // ...
    })
    .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;

        console.error(errorCode, errorMessage)
    });
    }

    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
            <View 
                style={styles.logo}
            />
            <Text style={styles.titleApp}>KamilApp</Text>
        </View>
        <View style={styles.titleContainer}>
            <Text style={styles.title}>Masuk</Text>
        </View>
        <View style={styles.formContainer}>
            <Text style={styles.formTitle}>Email</Text>
            <TextInput
                style={styles.form}
                placeholder= 'Masukkan Email'
                value= {email}
                onChangeText= {setEmail}
            />
            <Text style={styles.formTitle}>Password</Text>
            <TextInput
                style={styles.form}
                placeholder= 'Masukkan Password'
                value= {password}
                onChangeText= {setPassword}
                secureTextEntry= {true}
            />
        </View>
        <View>
            <Text style={styles.textDaftar} onPress={()=>navigation.navigate("Register")}>Belum punya akun? Daftar disini!</Text>
        </View>
        <View style={{justifyContent: 'center',alignItems: 'center'}}>
            <TouchableOpacity 
                style={styles.button}
                onPress={submit}
            >
            <Text style={{fontSize: Scale(20), color: 'white', fontWeight: 'bold'}}>Masuk</Text>    
            </TouchableOpacity>
        </View>
      </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        justifyContent: 'center'
    },
    logoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: Scale(20)
    },
    logo: {
        borderRadius: Scale(90),
        backgroundColor: '#034694',
        height: Scale(120),
        width: Scale(120),
    },
    titleApp: {
        fontSize: Scale(24),
        fontFamily: 'Roboto',
        color: '#034694',
    },
    titleContainer: {
        alignItems: 'center'
    },  
    title: {
        fontWeight: 'bold',
        fontSize: Scale(24),
        color: '#034694'
    },
    formContainer: {
        justifyContent: 'flex-start',
        marginHorizontal: Scale(15),
        marginVertical: Scale(15)
    },
    formTitle: {
        fontFamily: 'Roboto',
        fontSize: Scale(18),
        fontWeight: 'bold',
        color: '#ABABAB',
        marginTop: Scale(30)
    },
    form: {
        borderBottomColor: '#000000',
        borderBottomWidth: Scale(2),
        fontWeight: 'bold',
        fontSize: Scale(18),
    },
    textDaftar:{
        marginHorizontal: Scale(15), 
        fontSize: Scale(18), 
        fontWeight: 'bold',
        color: '#034694',
        marginBottom: Scale(100)
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#034694',
        borderRadius: Scale(30),
        height: Scale(64),
        width: Scale(332),
        marginHorizontal: Scale(15)
    }
})

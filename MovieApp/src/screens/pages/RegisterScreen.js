import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from "react-native";
import React, { useState } from "react";
import Form from "../../components/Form";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import Scale from '../../transforms/Scale'

export default function RegisterScreen({navigation}) {

  const [namaLengkap, setNamaLengkap] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [cPassword, setCPassword] = useState("");

  const submit = () => {

  const auth = getAuth();
  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in 
      const user = userCredential.user;
      navigation.navigate("Login")
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      // ..

    });
    }

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <Form
            label="Nama Lengkap"
            placeholder="Masukkan Nama Lengkap"
            value={namaLengkap}
            onChangeText={setNamaLengkap}
          />
          <Form
            label="Username"
            placeholder="Masukkan Username"
            value={username}
            onChangeText={setUsername}
          />
          <Form
            label="Email"
            placeholder="Masukkan Email"
            value={email}
            onChangeText={setEmail}
          />
          <Text style={styles.label}>Password</Text>
          <TextInput
            style={styles.input}
            placeholder="Masukkan Password"
            value={password}
            onChangeText={setPassword}
            secureTextEntry={true}
          />
          <Text style={styles.label}>Konfirmasi Password</Text>
          <TextInput
            style={styles.input}
            placeholder="Masukkan Konfirmasi Password"
            value={cPassword}
            onChangeText={setCPassword}
            secureTextEntry={true}
          />
          <TouchableOpacity 
            style={styles.button}
            onPress={submit}  
          >
            <Text style={styles.textDaftar}>Daftar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5F5F8",
  },
  header: {
    height: Scale(57),
    justifyContent: "center",
    backgroundColor: "white",
    marginBottom: Scale(5),
  },
  daftar: {
    color: "#034694",
    marginHorizontal: Scale(20),
    fontSize: Scale(18),
    fontWeight: "bold",
  },
  formContainer: {
    height: Scale(550),
    backgroundColor: "white",
  },
  label: {
    color: "#ABABAB",
    fontSize: Scale(18),
    fontWeight: "bold",
    marginHorizontal: Scale(10),
  },
  input: {
    backgroundColor: "#DEDEDE",
    borderWidth: Scale(1),
    marginHorizontal: Scale(10),
    height: Scale(50),
    borderRadius: Scale(10),
    color: "black",
    padding: Scale(10),
    marginBottom: Scale(15),
  },
  textDaftar: {
    fontSize: Scale(20),
    color: "white",
    fontWeight: "bold",
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#034694",
    borderRadius: Scale(30),
    height: Scale(64),
    width: Scale(332),
    marginHorizontal: Scale(15),
    marginTop: Scale(13),
  },
});

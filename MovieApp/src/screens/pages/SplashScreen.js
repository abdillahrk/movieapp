import { StyleSheet, Text, View } from "react-native";
import React, { useCallback, useEffect } from "react";
import { useFocusEffect } from "@react-navigation/native";
import * as SecureStore from "expo-secure-store";
import { useDispatch } from "react-redux";

import { me } from "../../api/me"
import { login } from "../../redux/authSlice";
import Scale from '../../transforms/Scale'

export const SplashScreen = ({ navigation }) => {

	const dispatch = useDispatch();

	useFocusEffect(
		useCallback(() => {
			const validateToken = async () => {
				try {
					const token = await SecureStore.getItemAsync("token");

					if (token) {
						// eslint-disable-next-line no-param-reassign
						console.log("Token Detected, Validating");
						const res = await me({ token });

						console.log("Token is Valid, logging in", res.data);
						dispatch(login({ token }));

						return;
					}

					// If no token go to login
					navigation.replace("Login");
				} catch (error) {
					console.log("Token Not Valid");
					SecureStore.deleteItemAsync("token");
					console.error(error);
					navigation.replace("Login");
				}
			};

			validateToken();
		}, []),
	);

	return (
		<View style={styles.container}>
            <View 
                style={styles.logo}
            />
			<Text style={styles.titleApp}>Welcome to Kamil App</Text>
		</View>
	);
};

const styles = StyleSheet.create({
    container: { 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center" 
    },
    titleApp: {
        fontSize: Scale(24),
        fontFamily: 'Roboto',
        color: '#034694',
    },
    logo: {
        borderRadius: Scale(90),
        backgroundColor: '#034694',
        height: Scale(120),
        width: Scale(120),
    },
});
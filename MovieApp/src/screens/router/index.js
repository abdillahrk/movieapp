import { View, Text } from "react-native";
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { useSelector } from "react-redux";

import AboutScreen from "../pages/AboutScreen";
import HomeScreen from "../pages/HomeScreen"
import RegisterScreen from "../pages/RegisterScreen";
import LoginScreen from "../pages/LoginScreen"
import { MaterialCommunityIcons } from "@expo/vector-icons";
import DetailScreen from "../pages/DetailScreen";

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createNativeStackNavigator();

export default function Router() {
  const isSignedIn = useSelector((state) => state.Auth.isSignedIn);
  return (
    <NavigationContainer>
        {isSignedIn? (
          <>
            <Tab.Navigator
              screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                  let iconName;

                  if (route.name === 'HomeApp') {
                    iconName = focused
                      ? 'home'
                      : 'home-outline';
                  } else if (route.name === 'About') {
                    iconName = focused ? 'account' : 'account-outline';
                  }

                  // You can return any component that you like here!
                  return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: 'black',
                tabBarInactiveTintColor: 'gray',
                headerShown: false,
              })}
            >
              <Tab.Screen 
                name="HomeApp" 
                component={HomeApp}
              />
              <Tab.Screen name="About" component={AboutScreen} />
            </Tab.Navigator>
          </>
        ) : (
          <>
          <Stack.Navigator>
          {/* <Stack.Screen name="SplashScreen" component={SplashScreen}/> */}
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          </Stack.Navigator>
          </>
        )}
    </NavigationContainer>
  );
}

const HomeApp = () => (
  <Stack.Navigator headerShown={false}>
    <Stack.Screen name="Home" component={HomeScreen}/>
    <Stack.Screen name="Detail" component={DetailScreen}/>
  </Stack.Navigator>
)